package au.com.bellsolutions.android.card.hardware;

import java.io.IOException;

import org.simalliance.openmobileapi.Channel;
import org.simalliance.openmobileapi.Reader;
import org.simalliance.openmobileapi.SEService;
import org.simalliance.openmobileapi.Session;

import android.content.Context;
import android.os.Looper;
import android.util.Log;
import au.com.bellsolutions.android.card.ISO7816Card.CardSpec;
import au.com.bellsolutions.android.emv.util.HexString;


/**
 * A Secure Element implementation of {@link com.visa.android.emv.Card.CardSpec}
 * @author tbelleng
 *
 */
public class SecureElement implements CardSpec, SEService.CallBack {
	private static final String TAG = "SecureElementInterface";
	protected static SEService mSEService;
	private Reader mReader;
	private Session mSession;
	private Channel mChannel;
	private Context ctx;
	private byte[] mATR;
	
	/**
	 * SecureElement constructor
	 * @param ctx The application context
	 */
	public SecureElement(Context ctx) {
		this.ctx = ctx;
	}
	
	/**
	 * ATR from the secure element
	 * @return the ATR
	 */
	public byte[] getATR() {
		return mATR;
	}
	
	/**
	 * Connects to the secure element through the SIMAlliance API.
	 * Connect is blocking and should not be called from the UI thread. An exception 
	 * will be thrown if it is used on the main thread.
	 * Implements {
	 * @throws IOException 
	 */
	public void connect() throws IOException {
		if (Looper.myLooper() == Looper.getMainLooper()) {
			// on the UI thread
			throw new RuntimeException("Connect should not be called on the main thread");
		}
		close();
		mSEService = new SEService(ctx, this);
		if (mSEService != null) {
			if (!mSEService.isConnected()) {
				synchronized(this) {
					try {
						Log.d(TAG, "Waiting service connection");
						wait();
						Log.d(TAG, "Service connected");
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			if (mSession == null) {
				Reader[] readers = mSEService.getReaders();
				for (Reader r : readers) {
					String name = r.getName();
					if (name.contains("SIM") || name.contains("UICC")) {
						mReader = r;
						Log.d(TAG, "Connected to UICC reader");
						mSession = mReader.openSession();
						mATR = mSession.getATR();
					}
				}
			}
		}
	}

	/**
	 * Shuts down the SEService object
	 */
	public void close() throws IOException {
		if (mSEService != null) {
			mSEService.shutdown();
		}
		mReader = null;
		mSession = null;
		mChannel = null;
		mSEService = null;
	}

	/**
	 * Sends and APDU to the secure element and returns the response and status word
	 */
	public byte[] send(byte[] apdu) throws IOException {
		if (mSession == null || mSession.isClosed()) {
			return null;
		} else {
			if (isSelect(apdu)) {
				byte[] aid = getAidFromApdu(apdu);
				mChannel = mSession.openLogicalChannel(aid);
				if (mChannel != null) {
					byte[] selResp = mChannel.getSelectResponse();
					if (selResp == null) {
						// select was successful but no select response ??
						selResp = new byte[] {(byte)0x90, 0x00};
					}
					return selResp;
				} else {
					// should return application not found
					return new byte[] {0x69, (byte)0x80};
				}
			} else {
				if (mChannel != null) {
					return mChannel.transmit(apdu);
				} else {
					return null;
				}
			}
		}
	}
	
	/**
	 * Find out whether or not the APDU is a SELECT APDU
	 * @param apdu
	 * @return true if the APDU represents a SELECT APDU using INS = 0xA4
	 */
	private boolean isSelect(byte[] apdu) {
		byte cmd = apdu[0];
		byte ins = apdu[1];
		
		if (cmd == 0x00 && ins == (byte)0xA4) {
			Log.d(TAG, "APDU is select");
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Retrieve the AID from a SELECT APDU
	 * @param apdu
	 * @return the AID
	 */
	private byte[] getAidFromApdu(byte[] apdu) {
		byte lc = apdu[4];
		byte[] aid = new byte[lc];
		System.arraycopy(apdu, 5, aid, 0, aid.length);
		Log.d(TAG, "AID retrieved: " + HexString.hexify(aid));
		return aid;
	}
	
	/**
	 * Called once the service is connected. the {@link #connect()} method blocks
	 * until this method is called by the SEService
	 */
	@Override
	public void serviceConnected(SEService serv) {
		synchronized(this) {
			notify();
		}
	}
}
